﻿using UnityEngine;
using System.Collections;


public class GameController : MonoBehaviour 
{
	public GameObject hazard;
	public Vector3 spawnValues; 
	public int hazardCount;

	public float spawnGameWait;  
	public float userStartWait; 
	public float waveAfterLoopWait; 

	void Start()
	{
		StartCoroutine (SpawnWaves());
	}

	IEnumerator SpawnWaves()
	{	

		yield return new WaitForSeconds(userStartWait);

		while(true)
		{
			for(int i= 0; i<hazardCount;i++)
			{
				Vector3 spawnPostion     = new Vector3(Random.Range(-spawnValues.x,spawnValues.x),spawnValues.y,spawnValues.z);
				Quaternion spawnRotation = Quaternion.identity;
		  		Instantiate(hazard,spawnPostion,spawnRotation);
		  		yield return new WaitForSeconds(spawnGameWait);
			}

			yield return new WaitForSeconds(waveAfterLoopWait);
		}

		
	}


	
}
